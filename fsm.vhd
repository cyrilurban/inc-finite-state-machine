-- file:    fsm.vhd
-- author:  CYRIL URBAN
-- date:    2015-04-26
-- brief:   Finite State Machine


library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is (TEST0, TEST1, TEST2, TEST3, TEST4, TEST5, TEST6A, TEST6B, TEST7A, TEST7B, TEST8A, TEST8B, TEST9, TEST10, PRINT_MESSAGE, PRINT_ERROR, TEST_ERROR, FINISH);
   signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= TEST0;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
--TEST C:0123456789
-------------------
--kodA = 3051036329
--kodB = 3051060549


next_state_logic : process(present_state, KEY, CNT_OF)
begin
   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   -- TEST0: 3
	when TEST0 =>
      next_state <= TEST0;
      if (KEY(3) = '1') then -- kdyz je zmackunta klavesa 3, tak mohu testovat dalsi klavesu (nahraji: next_state <= TEST1)
			next_state <= TEST1;
		elsif (KEY(15) = '1') then -- kdyz je zmackunta klavesa # (15), tak vytisknu skoncim errorem (nahraji: next_state <= PRINT_ERROR)
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then -- kdyz je zmackunta jina klavesa nez 3, tak nahlasim chybu (nahraji: next_state <= TEST_ERROR)
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
   -- TEST1: 0
	when TEST1 =>
      next_state <= TEST1;
      if (KEY(0) = '1') then
			next_state <= TEST2;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
	-- TEST2: 5
	when TEST2 =>
      next_state <= TEST2;
      if (KEY(5) = '1') then
			next_state <= TEST3;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
	-- TEST3: 1
   when TEST3 =>
      next_state <= TEST3;
      if (KEY(1) = '1') then
			next_state <= TEST4;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
	-- TEST4: 0
   when TEST4 =>
      next_state <= TEST4;
      if (KEY(0) = '1') then
			next_state <= TEST5;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
   -- TEST5: 3 nebo 6 (nasleduje "vetveni" - protoze druha cast kodu neni totozna)
	when TEST5 =>
      next_state <= TEST5;
      if (KEY(3) = '1') then -- kdyz 3 (korektni test z A), dale pokracuje s TEST6A (nahraji: next_state <= TEST6A)
			next_state <= TEST6A;
		elsif (KEY(6) = '1') then -- kdyz 6 (korektni test z B), dale pokracuje s TEST6A (nahraji: next_state <= TEST6B)
			next_state <= TEST6B;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
   -- TEST6A: 6
	when TEST6A =>
      next_state <= TEST6A;
      if (KEY(6) = '1') then
			next_state <= TEST7A;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
   -- TEST7A: 3
	when TEST7A =>
      next_state <= TEST7A;
      if (KEY(3) = '1') then
			next_state <= TEST8A;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
   -- TEST8A: 2
	when TEST8A =>
      next_state <= TEST8A;
      if (KEY(2) = '1') then
			next_state <= TEST9; -- dale pokracuji jen TEST9 (protoze tato polozka je stejna pro kod A i pro kod B)
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
	-- TEST6B: 0
   when TEST6B =>
      next_state <= TEST6B;
      if (KEY(0) = '1') then
			next_state <= TEST7B;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -		
   -- TEST7B: 5
	when TEST7B =>
      next_state <= TEST7B;
      if (KEY(5) = '1') then
			next_state <= TEST8B;
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
   -- TEST8B: 4
	when TEST8B =>
      next_state <= TEST8B;
      if (KEY(4) = '1') then
			next_state <= TEST9; -- dale pokracuji jen TEST9 (protoze tato polozka je stejna pro kod A i pro kod B)
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
   -- TEST9: 9
	when TEST9 =>
      next_state <= TEST9;
      if (KEY(9) = '1') then
			next_state <= TEST10; -- jeste nutny TEST10
		elsif (KEY(15) = '1') then
         next_state <= PRINT_ERROR; 
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
   -- TEST10: # (je konec?)
	when TEST10 =>
      next_state <= TEST10;
      if (KEY(15) = '1') then -- testuji, zda jsem nakonec zmacknul potvrzujici tlacitko(15) - #.
         next_state <= PRINT_MESSAGE; -- jestli ano dam pokyn k poveleni pristupu (nahraji: next_state <= PRINT_MESSAGE)
		elsif (KEY(14 downto 0) /= "000000000000000") then
			next_state <= TEST_ERROR;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -	
	-- PRINT_MESSAGE (pristup povolen)
   when PRINT_MESSAGE =>
      next_state <= PRINT_MESSAGE;
      if (CNT_OF = '1') then -- az vypisu vsechny znaky (CNT_OF = '1'), tak prejdu na konec (nahraji: next_state <= FINISH)
         next_state <= FINISH;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   -- TEST_ERROR
	when TEST_ERROR =>
      next_state <= TEST_ERROR; 
      if (KEY(15) = '1') then -- jsem v TEST_ERROR dokud nezmacku klavesu(15) - #, pak error vytiskny (next_state <= PRINT_ERROR)
         next_state <= PRINT_ERROR; 
      end if;
	-- - - - - - - - - - - - - - - - - - - - - - -
	-- PRINT_ERROR
   when PRINT_ERROR =>
      next_state <= PRINT_ERROR;
      if (CNT_OF = '1') then -- az vypisu vsechny znaky (CNT_OF = '1'), tak prejdu na konec (nahraji: next_state <= FINISH)
         next_state <= FINISH; 
      end if;
	-- - - - - - - - - - - - - - - - - - - - - - -
   -- FINISH
	when FINISH =>
      next_state <= FINISH;
      if (KEY(15) = '1') then -- jsem ve FINISH dokud nezmacku klavesu(15) - #, pak se vratim znuvu na zacatek (nahraji: next_state <= TEST0)
         next_state <= TEST0; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0'; -- clock enable sign�l ��ta�e
   FSM_MX_MEM     <= '0'; -- multipexor pro v�b�r pam�ti
   FSM_MX_LCD     <= '0'; -- multiplexor pro v�b�r vstupu na LCD
   FSM_LCD_WR     <= '0'; -- z�pis znaku na LCD
   FSM_LCD_CLR    <= '0'; -- vymazan� LCD

   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
	-- PRINT_MESSAGE (pristup povolen)
	when PRINT_MESSAGE =>
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
		FSM_MX_MEM		<= '1'; -- hodnota multipexoru pro v�b�r pam�ti na '1': "pristup povolen"
   -- - - - - - - - - - - - - - - - - - - - - - -
   -- PRINT_ERROR (pristup odepren)
	when PRINT_ERROR =>
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
		FSM_MX_MEM		<= '0'; -- hodnota multipexoru pro v�b�r pam�ti na '0': "pristup odepren"
   -- - - - - - - - - - - - - - - - - - - - - - -
   -- FINISH
	when FINISH =>
      if (KEY(15) = '1') then -- ceka pouze na #, jinak nereaguje
         FSM_LCD_CLR    <= '1'; -- vymaze LCD
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   -- other
	when others =>
      if (KEY(14 downto 0) /= "000000000000000") then -- klavesy 0-14 budou psat "*" (FSM_LCD_WR <= '1')
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then -- pristisku klavesy 15 (#) smazu LCD (vsechny "*"). (nahraji: FSM_LCD_CLR <= '1')
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
	end case;
end process output_logic;

end architecture behavioral;

